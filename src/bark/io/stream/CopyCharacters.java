package bark.io.stream;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * CopyCharacters class
 *
 * @author bark
 * @date 2018/7/9.
 */
public class CopyCharacters {
    public static void main(String[] args) throws IOException{
        FileReader in = null;
        FileWriter out = null;
        try {
            in = new FileReader("xanadu.txt");
            out = new FileWriter("characteroutput.txt");
            int c;
            //在字符流中，int最后16位保存一个字符，而在字节流中，int在最后8位保存一个字节（int 32位）
            while ((c = in.read()) != -1) {
                out.write(c);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }
    }
}
