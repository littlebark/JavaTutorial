package bark.io.stream;

import java.io.*;

/**
 * BufferStreamTest class
 *
 * @author bark
 * @date 2018/7/9.
 * 为什么要有缓冲流？
 * 字符流和字节流每个读取或写入请求都由底层操作系统直接处理，所以经常触发磁盘操作或者网络访问，开销大，效率低
 * 缓冲流是将其放入内存，只有将缓冲区空或者满时调用底层操作API输入和输出
 */
public class BufferStreamTest {
    public static void main(String[] args) throws IOException{
        //不设定缓冲区大小默认8192
        try (
            InputStream in = new BufferedInputStream(new FileInputStream("xanadu.txt"));
            OutputStream out = new BufferedOutputStream(new FileOutputStream("outagain.txt"));
        ){
            int c;
            while((c = in.read()) != -1){
                //不带参数的write当缓冲区满时会刷新缓冲区，close时也会自动刷新缓冲区。
                out.write(c);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //这边改用try-with-resources语法，对于实现了AutoCloseable的任何对象（包括实现Closeable的所有对象）
        //都能将资源放在try后面的括号里，系统会自动帮你关闭资源(JDK1.7及以上)
    }
}
