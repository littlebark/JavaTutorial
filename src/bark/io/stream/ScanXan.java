package bark.io.stream;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

/**
 * ScanXan class
 *
 * @author bark
 * @date 2018/7/9.
 */
public class ScanXan {
    //搬家到gitlab了测试改动
    public static void main(String[] args) throws Exception{
        Scanner s = null;

        try {
            s = new Scanner(new BufferedReader(new FileReader("xanadu.txt")));

            while (s.hasNext()) {
                System.out.println(s.next());
            }
        } finally {
            if (s != null) {
                s.close();
            }
        }
    }
}
