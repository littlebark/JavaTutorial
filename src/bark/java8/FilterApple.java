package bark.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * @Author littlebark
 * @Date 2018/8/2.
 * @Description
 */
public class FilterApple {
    static class Apple{
        Apple(){

        }
        public String getColor(){
            return "color";
        }
        public int getWeight(){
            return 0;
        }

    }
    public static boolean isGreenApple(Apple apple) {
        return "green".equals(apple.getColor());
    }
    public static boolean isHeavyApple(Apple apple) {
        return apple.getWeight() > 150;
    }
    public interface Predicate<T>{
        boolean test(T t);
    }
    static List<Apple> filterApples(List<Apple> inventory,
                                    Predicate<Apple> p) {
        List<Apple> result = new ArrayList<>();
        for (Apple apple: inventory){
            if (p.test(apple)) {
                result.add(apple);
            }
        }
        return result;
    }
    public static List<Apple> filterGreenApples(List<Apple> inventory){
        List<Apple> result = new ArrayList<>();
        for (Apple apple: inventory){
            if ("green".equals(apple.getColor())) {
                result.add(apple);
            }
        }
        return result;
    }
    public static List<Apple> filterHeavyApples(List<Apple> inventory){
        List<Apple> result = new ArrayList<>();
        for (Apple apple: inventory){
            if (apple.getWeight() > 150) {
                result.add(apple);
            }
        }
        return result;
    }
    public static void main(String[] args) {
        Supplier<Apple> apple  = Apple::new;
        Apple c1 = apple.get();
    }
}
