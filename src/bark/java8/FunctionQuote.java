package bark.java8;

import java.util.function.Function;

/**
 * @Author littlebark
 * @Date 2018/8/2.
 * @Description
 */
public class FunctionQuote {
    static class Letter{
        public static String addHeader(String text){
            return text+":head";
        }
        public static String addEnd(String text){
            return text+":end";
        }
    }

    public static void main(String[] args) {
        Function<String,String> addHeader = Letter::addHeader;
        Function<String,String> addHeader1 = (String s) -> Letter.addHeader(s);
        Function<String,String> addEnd = addHeader.andThen(Letter::addEnd);
        System.out.println(addEnd.apply("bark"));
    }
}
