package bark.java8;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 * @Author littlebark
 * @Date 2018/8/2.
 * @Description
 */
public class ConsumerTest {
    public static <T> void forEach(List<T> list, Consumer<T> c){
        for(T i: list){
            c.accept(i);
        }
    }

    public static void main(String[] args) {
        forEach(Arrays.asList("lambdas","in","action"),(String i) -> System.out.println(i));
    }
}
