package bark.java8.streamdemo;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @Author littlebark
 * @Date 2018/8/3.
 * @Description
 */
public class MapTest {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("hello","world");
        List<String> lists = list.stream()
                .map((word) -> word.split(""))
                .flatMap(Arrays::stream)
                .distinct().collect(toList());
        System.out.println(lists);
    }
}
