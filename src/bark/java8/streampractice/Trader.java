package bark.java8.streampractice;

/**
 * @Author littlebark
 * @Date 2018/8/6.
 * @Description
 */
public class Trader {
    private final String name;
    private final String city;
    public Trader(String n, String c){
        this.name = n;
        this.city = c;
    }
    public String getName(){
        return this.name;
    }
    public String getCity(){
        return this.city;
    }
    @Override
    public String toString(){
        return "交易员:"+this.name + " 在 " + this.city;
    }
}
