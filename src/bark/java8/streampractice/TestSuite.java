package bark.java8.streampractice;

import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * @Author littlebark
 * @Date 2018/8/6.
 * @Description
 */
public class TestSuite {
    public static void main(String[] args) {
        List<Transaction> transactions = new Transactions().getTransactions();
        //1.找出2011年发生的所有交易，并按照交易额排序（从低到高）
        System.out.println("1.找出2011年发生的所有交易，并按照交易额排序（从低到高）");
        System.out.println(
            transactions.stream()
                    .filter(d->d.getYear() == 2011)
                    .sorted(Comparator.comparing(Transaction::getValue).reversed())
                    .collect(toList())
        );
        System.out.println();
        //2.交易员都在哪些不同的城市工作过？
        System.out.println("2.交易员都在哪些不同的城市工作过？");
        System.out.println(
            transactions.stream()
                .map((transaction)->transaction.getTrader().getCity())
                .distinct()
                .collect(toList())
        );
        System.out.println();
        //3.查找所有来自于剑桥的交易员，并按姓名排序。
        System.out.println("3.查找所有来自于剑桥的交易员，并按姓名排序。");
        System.out.println(
            transactions.stream().filter((transaction)->"剑桥".equals(transaction.getTrader().getCity()))
                .map((d)->d.getTrader().getName())
                    .distinct()
                    .sorted()
                .collect(toList())
        );
        System.out.println();
        //4.返回所有交易员的姓名字符串，按字母顺序排序。
        System.out.println("4.返回所有交易员的姓名字符串，按字母顺序排序。");
        System.out.println(
            transactions.stream()
                .map(d->d.getTrader().getName())
                .distinct()
                .sorted().collect(toList())
        );
        System.out.println();
        //5.有没有交易员是在米兰工作的？
        System.out.println("5.有没有交易员是在米兰工作的？");
        System.out.println(
            transactions.stream()
                .filter(t->"米兰".equals(t.getTrader().getCity()))
                .map(d->d.getTrader().getName())
                .distinct()
                .collect(toList())
        );
        System.out.println();
        //6.打印生活在剑桥的交易员的所有交易额。
        System.out.println("6.打印生活在剑桥的交易员的所有交易额。");

            transactions.stream()
                .filter((transaction)->"剑桥".equals(transaction.getTrader().getCity()))
                .map(t->t.getValue())
                .forEach(System.out::println)
        ;
        System.out.println();
        //7. 所有交易中，最高的交易额是多少？
        System.out.println("7. 所有交易中，最高的交易额是多少？");
        System.out.println(
            transactions.stream()
                .map(e->e.getValue())
                .reduce(Integer::max)
        );
        System.out.println();
        //8.找到交易额最小的交易。
        System.out.println("8.找到交易额最小的交易。");
        System.out.println(
            transactions.stream()
                .reduce((t,d)->t.getValue()>d.getValue()? d:t)
        );
    }
}
