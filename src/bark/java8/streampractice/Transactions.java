package bark.java8.streampractice;

import java.util.Arrays;
import java.util.List;

/**
 * @Author littlebark
 * @Date 2018/8/6.
 * @Description
 */
public class Transactions {
    private List<Transaction> transactions = null;
    public Transactions(){
        Trader raoul = new Trader("拉乌尔", "剑桥");
        Trader mario = new Trader("马里奥","米兰");
        Trader alan = new Trader("阿兰","剑桥");
        Trader brian = new Trader("布莱恩","剑桥");
        transactions = Arrays.asList(
            new Transaction(brian, 2011, 300),
            new Transaction(raoul, 2012, 1000),
            new Transaction(raoul, 2011, 400),
            new Transaction(mario, 2012, 710),
            new Transaction(mario, 2012, 700),
            new Transaction(alan, 2012, 950)
        );
    }
    public List<Transaction> getTransactions(){
        return this.transactions;
    }
}
