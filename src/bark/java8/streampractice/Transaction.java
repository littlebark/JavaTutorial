package bark.java8.streampractice;

/**
 * @Author littlebark
 * @Date 2018/8/6.
 * @Description
 */
public class Transaction {
    private final Trader trader;
    private final int year;
    private final int value;
    public Transaction(Trader trader, int year, int value){
        this.trader = trader;
        this.year = year;
        this.value = value;
    }
    public Trader getTrader(){
        return this.trader;
    }
    public int getYear(){
        return this.year;
    }
    public int getValue(){
        return this.value;
    }
    @Override
    public String toString(){
        return "{" + this.trader + ", " +
                "年份: "+this.year+", " +
                "交易金额:" + this.value +"}";
    }
}
