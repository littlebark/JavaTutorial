package bark.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * @Author littlebark
 * @Date 2018/8/2.
 * @Description
 */
public class FilterString {
    public static <T> List<T> filter(List<T> list, Predicate<T> p) {
        List<T> results = new ArrayList<>();
        for(T s: list){
            if(p.test(s)){
                results.add(s);
            }
        }
        return results;
    }
    //Predicate<String> nonEmptyStringPredicate = (String s) -> !s.isEmpty();



    public static void main(String[] args) {
        Predicate<String> nonEmptyStringPredicate = (String s ) -> !s.isEmpty();
        List<String> list = new ArrayList<String>();
        list.add("a");
        list.add("b");
        list.add("");
        List<String> nonEmpty = filter(list, nonEmptyStringPredicate);
        System.out.println(nonEmpty);
    }
}
