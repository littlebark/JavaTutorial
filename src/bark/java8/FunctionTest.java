package bark.java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * @Author littlebark
 * @Date 2018/8/2.
 * @Description
 */
public class FunctionTest {
    public static <T, R> List<R> map(List<T> list, Function<T, R> f) {
        List<R> result = new ArrayList<>();
        for(T s: list){
            result.add(f.apply(s));
        }
        return result;
    }

    public static void main(String[] args) {
        List<Integer> l = map(
                Arrays.asList("lambdas","in","action"),
                (String s) -> s.length()
        );
        Function<Integer,Integer> add1 = x -> x + 1;
        Function<Integer,Integer> mulply2 = x -> x * 2;
        Function<Integer,Integer> andthen = add1.andThen(mulply2);
        Function<Integer,Integer> compose = add1.compose(mulply2);

        System.out.println(andthen.apply(2));
        System.out.println(compose.apply(2));
    }
}
