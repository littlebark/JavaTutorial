package bark.fileio.stream;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * @Author littlebark
 * @Date 2018/7/10.
 * @Description
 */
public class FileRead {
    public static void main(String[] args) throws IOException {
        //小文件的读取一般用readAllBytes(Path)或者readAllLines(Path, Charset)
        Path path = Paths.get("D://doc.txt");
        //list里的每一个单位装一行数据
        List<String> li = Files.readAllLines(path, StandardCharsets.UTF_8);
        //字节是这样
        byte[] fileArray;
        fileArray = Files.readAllBytes(path);
        //newBufferReader用法类似
        Charset charset = Charset.forName("US-ASCII");
        try (BufferedReader reader = Files.newBufferedReader(path, charset)) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
        //类似的还有 newInputStream(Path, OpenOption...)

        //创建文件的操作create file
        try {
            // Create the empty file with default permissions, etc.
            Files.createFile(path);
        } catch (FileAlreadyExistsException x) {
            System.err.format("file named %s" +
                    " already exists%n", path);
        } catch (IOException x) {
            // Some other sort of failure, such as permissions.
            System.err.format("createFile error: %s%n", x);
        }
    }
}
