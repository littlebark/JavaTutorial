package bark.fileio.stream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * @Author littlebark
 * @Date 2018/7/10.
 * @Description
 */
public class CopyAFile {
    public static void main(String[] args) {
        //用files类进行复制很方便
        Path target2 = Paths.get("C:\\copyed.txt");
        try {
            Files.copy(Paths.get("D:\\road.enex"), target2, REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
