package bark.fileio.stream;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @Author littlebark
 * @Date 2018/7/10.
 * @Description
 */
public class PathTest {
    public static void main(String[] args) {
        Path path = Paths.get("C:\\home\\joe\\foo\\a.txt");


        System.out.format("toString: %s%n", path.toString());
        System.out.format("getFileName: %s%n", path.getFileName());
        System.out.format("getName(0): %s%n", path.getName(0));
        System.out.format("getNameCount: %d%n", path.getNameCount());
        System.out.format("subpath(0,2): %s%n", path.subpath(0,2));
        System.out.format("getParent: %s%n", path.getParent());
        System.out.format("getRoot: %s%n", path.getRoot());


        Path relativePath = Paths.get("sally\\bar");
        System.out.format("相对toString: %s%n", relativePath.toString());
        System.out.format("相对getFileName: %s%n", relativePath.getFileName());
        System.out.format("相对getName(0): %s%n", relativePath.getName(0));
        System.out.format("相对getNameCount: %d%n", relativePath.getNameCount());
        System.out.format("相对subpath(0,2): %s%n", relativePath.subpath(0,2));
        System.out.format("相对getParent: %s%n", relativePath.getParent());
        System.out.format("相对getRoot: %s%n", relativePath.getRoot());
        //相对路径对比root为空
        relativePath.resolve("aa");
        System.out.println(relativePath.toString());

    }
}
