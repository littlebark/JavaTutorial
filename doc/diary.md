## 通用笔记 ##
### 6.27 ###
1. 注解
    关键字@interface
    注解里可以定义各种基本类型的属性（类，数组）
2. 注解的使用
    反射。
    核心在于class，field，method等都继承了注解的接口AnnotationElement。
    使用时，可以用这个接口很方便的获取一个类的方法，字段，类上是否标记了注解。
    如果标记了，则实例化该注解，获得注解的属性，然后设置给这个反射获得的类的实例。
3. 一句话解释设计模式六大原则<br>
    a:单一职责。把功能分工，特定的类只干特定的事。
    b:开放封闭原则。利用抽象，对功能的变动，不改原代码，通过新增代码解决问题。
    c:里式替换原则。所有父类能出现的地方，用子类替代也能正常运行。子类重写的方法入参要比父类宽松。比如父类入参是HashMap，子类入参可以是map。这样就可以替代。<br>
    d:依赖倒置。核心就是面向接口编程。
    e:接口隔离原则。把不同功能分给不同的接口，让实现类避免少了解与己无关的方法、通过实现不同接口保证与外部的耦合；
    f:迪米特原则。一个类对自己需要耦合或调用的类知道的最少，你(被耦合或调用的类)的内部是如何复杂和我没有关系，我就知道你提供的public方法，我只调用这些方法，其它的我不关心。只与直接朋友通信
### 7.15 ###
#### servlet ####
正常我们实现请求是要写很多Servle实现类来继承HTTPServlet，然后web.xml做如下配置，请求由Url-pattern进入，然后到对应的类。
`
<servlet>
        <servlet-name>HelloWorld</servlet-name>
        <servlet-class>HelloServlet</servlet-class>
</servlet>
`
`<servlet-mapping>
        <servlet-name>HelloWorld</servlet-name>
        <url-pattern>/HelloWorld</url-pattern>
    </servlet-mapping>
`
有了SpringMVC后，他就一个Servlet，请求到了SpringMVC对应的DispatcherServlet，他再去转发。
### 7.16 ###
#### interrrupt() ####
1. 如果线程是处于阻塞状态的，调用interrupt()会让线程中断标志为true。抛出InterruptedException，并且将状态设置成false.<br>
2. 如果线程不是阻塞状态的，他仅仅是标记成true，后续工作还需要程序自己处理。
比如，用if或者while不断判断isInterrupted()然后再做决策。
